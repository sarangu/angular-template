import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {
  @Input('open') drawer;
  @Output() closed = new EventEmitter();
 
  constructor() { }

  ngOnInit() {
    
  }


  close(){

    this.closed.emit(false)

  }


}
