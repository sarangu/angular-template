import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
@Input("input") input;
@Input("property") property;
@Output() onSearch = new EventEmitter();
  constructor() { 

  }

  ngOnInit() {
  }
  search(event){
    const val = event.target.value.toLowerCase();
    const temp = this.input.filter(d=>{
      return d[this.property].toLowerCase().indexOf(val) !== -1 || !val;
    })
    this.onSearch.emit(temp)

  }

}
 