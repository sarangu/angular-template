import { Component, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => NumberComponent),
    multi: true
  }]
})
export class NumberComponent implements ControlValueAccessor {
  value: any;
  numbers = /^[-+]?[0-9]+$/;
  onChange: (e) => void;

  writeValue(obj: any): void {


  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {

  }
  setDisabledState?(isDisabled: boolean): void {

  }

  constructor() { }

  ngOnInit() {
  }

  changer(e) {
    var val = e.target.value;

    this.onChange(val)
  }

}
