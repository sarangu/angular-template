import { Component, OnChanges,Input } from '@angular/core';
@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent implements OnChanges {
  @Input('steps') steps;
  @Input('active') active;
  index = 0;
  constructor() { }
  ngOnChanges() {
    this.steps.forEach((item,i) => {
      if(item.slug === this.active){
       this.index = i + 1}
    });
    this.steps.forEach((item,i) =>{
      if(i < this.index){
        this.steps[i]['active'] = true;
      }else{
        this.steps[i]['active'] = false;
      }
    })
  }
}
