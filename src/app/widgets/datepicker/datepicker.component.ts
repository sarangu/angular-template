import { Component, forwardRef, Input, ChangeDetectorRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
@Component({
  selector: "datepicker",
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    }
  ]
})
export class DatepickerComponent implements ControlValueAccessor {
  @Input("minDate") minDate;
  @Input("large")  large;
  @Input("readonly")  readonly;
  _minDate;
  value = '';
  disabled: boolean;
  onChanged: any = () => {};
  onTouched: any = () => {};
  constructor(private cd: ChangeDetectorRef) {}
  writeValue(val) {
    this.value = val;
  }
  registerOnChange(fn: any) {
    this.onChanged = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  addzero(n) {
    return n < 10 ? "0" + n : "" + n;
  }
  gettdate(val) {
    let d = new Date(val);
    return `${d.getFullYear()}-${this.addzero(
      d.getMonth() + 1
    )}-${this.addzero(d.getDate())}`;
  }
  onValueChange(e) {
    let date = this.gettdate(e);
    this.onChanged(date);
  }
  readonlyfn(e){
    e.preventDefault()
  }

  ngOnChanges(changes) {
    if(this.minDate){
      let d = new Date(changes.minDate.currentValue);
  d.setDate(d.getDate() + 30);
  this._minDate  = d;
  }
}
}
