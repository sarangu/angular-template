
import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from 'src/app/utils/services/loader.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})



export class LoaderComponent implements OnInit, OnDestroy {
  logostate = true;
  loaded = false;
  subscription: Subscription
  constructor(private loader: LoaderService) { }
  ngOnInit() {
    this.subscription = this.loader.loader.subscribe(data => {
      this.loaded = data;
    })

  }
  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

}
