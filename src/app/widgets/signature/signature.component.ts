import { Component, OnInit,ViewChild,forwardRef,Input } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ControlValueAccessor,NG_VALUE_ACCESSOR } from '@angular/forms';
@Component({
  selector: 'app-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.scss'],
  providers:[  {
    provide: NG_VALUE_ACCESSOR, 
    useExisting: forwardRef(() => SignatureComponent),
    multi: true     
  } ]
})
export class SignatureComponent implements ControlValueAccessor {
  @ViewChild(SignaturePad, { static: false }) signaturePad: SignaturePad;
  @Input('err') err;
  sp_options: Object = {
    'canvasWidth': 250,
    'canvasHeight': 100
  };
  value:string; 
  onChange:(e)=> void;
  onTouched:()=> void;
  disabled:boolean;

  constructor() { }

  writeValue(obj: any): void {

    this.value = obj;
    
  }
  registerOnChange(fn: any): void {
 this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  this.disabled = isDisabled
  }
  
  onClear(){
    this.signaturePad.clear();
    this.onChange('');
  }

}
