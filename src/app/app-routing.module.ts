import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './utils/auth.guard';
import { AccessGuard } from './utils/access.guard';
import { FourZeroFourComponent } from './four-zero-four/four-zero-four.component';



const routes: Routes = [
  {
  path:"",
  loadChildren:() => import('./modules/user/user.module').then(module => module.UserModule),
  canActivate:[AccessGuard]
},
{
  path:"admin",
  loadChildren:() => import('./modules/admin/admin.module').then(module => module.AdminModule),
  canActivate:[AuthGuard]
},
{
  path:"superadmin",
  loadChildren:() => import('./modules/super-admin/super-admin.module').then(module => module.SuperAdminModule)
},

{
  path:'404',
  component:FourZeroFourComponent
},{
  path: '**',
  redirectTo: '/404'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{scrollPositionRestoration:'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
