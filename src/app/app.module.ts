import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './utils/auth.guard';
import { FourZeroFourComponent } from './four-zero-four/four-zero-four.component';
import { LoaderComponent } from './widgets/loader/loader.component';
import { CommonService } from './utils/services/common.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AccessGuard } from './utils/access.guard';
import { HI } from './utils/http.interceptor';
import { Loader } from './utils/loader.interceptor';
import { LoaderService } from './utils/services/loader.service';






@NgModule({
  declarations: [
    AppComponent,
    FourZeroFourComponent,
    LoaderComponent
    
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({ 
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      timeOut: 5000,
      enableHtml:true,
      progressBar:true
    }),
    AppRoutingModule    
  ],
  providers: [
    AccessGuard,
    AuthGuard,
    CommonService,
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: HI, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: Loader, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
