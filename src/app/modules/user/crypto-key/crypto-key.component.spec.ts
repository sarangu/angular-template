import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CryptoKeyComponent } from './crypto-key.component';

describe('CryptoKeyComponent', () => {
  let component: CryptoKeyComponent;
  let fixture: ComponentFixture<CryptoKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CryptoKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
