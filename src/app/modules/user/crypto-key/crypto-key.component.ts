import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators,FormGroup } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/utils/services/common.service';
import { UserService } from 'src/app/utils/services/user.service';
@Component({
  selector: 'app-crypto-key',
  templateUrl: './crypto-key.component.html',
  styleUrls: ['./crypto-key.component.scss']
})
export class CryptoKeyComponent implements OnInit {
  submitted = false;
  form = this.fb.group({
    firstname: ['testingtest', Validators.required],
    lastname: ['testing', Validators.required],
    email: ['test01@gmail.com', [Validators.required, Validators.email]],
    mobile: ['1234567890', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")]],
    company_name: ['testtesttesttest', Validators.required],
    password: ['test@123', Validators.required],
    confirmPassword: ['test@123', Validators.required],
 },{validator: this.mismatch});

  mismatch(frm: FormGroup) {
  return frm.controls['password'].value === frm.controls['confirmPassword'].value ? null : {'mismatch': true};
}
  constructor(private router: Router,private route:ActivatedRoute, private fb: FormBuilder, private common:CommonService,private user:UserService) { 

    this.route.queryParams.subscribe(data=>{

      this.form.patchValue(data);

    })

  }

  ngOnInit() {
    console.log(this.form)
  }
  keygen() {
    this.submitted = true;
    if (this.form.status == 'VALID') {
         this.user.signUp(this.form.value).subscribe(data => {
        console.log(data) 
        this.router.navigate(['/admin/getting-started']);
      
      })
     
      
    }
  }
}

