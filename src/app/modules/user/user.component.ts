import { Component, OnInit } from '@angular/core';
import { animations } from '../../utils/animations';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations:animations
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
