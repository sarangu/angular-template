import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../../utils/services/common.service'
import { UserService } from 'src/app/utils/services/user.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  submitted = false;
  form = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    mobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")]],
    companyName: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  }, { validator: this.mismatch })
  constructor(private router: Router, private fb: FormBuilder, private common: CommonService,private user:UserService) { }
  ngOnInit() {
  }
  mismatch(frm: FormGroup) {
    return frm.controls['password'].value === frm.controls['confirmPassword'].value ? null : { 'mismatch': true };
  }
  signup() {
    this.submitted = true;
    if (this.form.status == 'VALID') {
      this.user.signUp(this.form.value).subscribe(res => {
        this.router.navigate(['/sign-in']);
        this.common.toast.fire({
          icon: 'success',
          title: 'Registered  successfully'
        })
      })
    }
  }
}
