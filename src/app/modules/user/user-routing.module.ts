import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { UserComponent } from './user.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { CryptoKeyComponent } from './crypto-key/crypto-key.component';


const routes: Routes = [
  {
    path:"",
    component:UserComponent,
    children:[
      {
        path:'sign-in',
        component:SignInComponent
      },
      {
        path:'sign-up',
        component:SignUpComponent
      },{
        path:'crypto-key',
        component:CryptoKeyComponent
      },{
        path:'',
        redirectTo:'/sign-in',
        pathMatch:'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
