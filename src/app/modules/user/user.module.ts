import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in/sign-in.component';
import { UserComponent } from './user.component';
import { UserRoutingModule } from './user-routing.module';
import { SignUpComponent } from './sign-up/sign-up.component';
import { CryptoKeyComponent } from './crypto-key/crypto-key.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/utils/shared.module';
import { CommonService } from 'src/app/utils/services/common.service';
import { UserService } from 'src/app/utils/services/user.service';


@NgModule({
  declarations: [SignInComponent, UserComponent, SignUpComponent, CryptoKeyComponent],
  imports: [
    UserRoutingModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  providers:[CommonService,UserService]
})
export class UserModule { }
