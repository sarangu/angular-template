import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/utils/services/common.service';
import { UserService } from 'src/app/utils/services/user.service';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  submitted = false;
  form = this.fb.group({
    email: ['', Validators.required],
    password: ['', [Validators.required]]
  })
  constructor(private router: Router, private fb: FormBuilder, private common: CommonService, private user:UserService) {
  }
  ngOnInit() {
  }
  signin() {
    this.submitted = true;
    if (this.form.status == 'VALID') {
      this.user.signIn(this.form.value).subscribe(res => {
        if (res['success'] == true) {
          this.common.setStorage('token',res['token']);        
          this.router.navigate(['/admin']);
          this.common.toster("Signed in successfully", "Done!",'success')
    
        }
      })
    }
  }
}
