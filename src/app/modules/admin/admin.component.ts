import { Component, OnInit } from '@angular/core';
import { animations } from '../../utils/animations';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  animations:animations
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

 
}
