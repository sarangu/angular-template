import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../../../utils/services/common.service";
import { AdminService } from 'src/app/utils/services/admin.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  companies: any = [];
  constructor(private common: CommonService, private admin: AdminService) { }
  ngOnInit() {
    this.read()
    this.common.sidebarEmmiterSubscribe().subscribe(data => {
      this.read()
    })
  }
  read() {
    this.admin.liquidSolutionsAll().subscribe(res => {
      this.companies = res;
      this.companies = this.companies.reverse()
    })
  }
}
