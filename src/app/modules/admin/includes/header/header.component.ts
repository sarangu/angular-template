import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/utils/services/common.service';
import { AdminService } from 'src/app/utils/services/admin.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menu = false;
  user = ''
  constructor(private router: Router, private common: CommonService, private admin: AdminService) {
  }
  ngOnInit() {
    this.read()
  }
  read() {
    this.admin.user().subscribe(res => {
      this.user = res['email'].split('@')[0];
      console.log(res)
    
    })
  }
  menuOpen() {
    this.menu = true;
  }
  menuClose(e) {
    this.menu = e;
  }
  logout(e) {
    e.preventDefault();
this.common.deleteStorage("token");
    this.router.navigate(['/sign-in']);
    this.common.toster("Signed out successfully", "Done!",'success')

  
  }
}
