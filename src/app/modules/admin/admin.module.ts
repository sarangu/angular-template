import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { AdminRoutingModule } from "./admin-routing.module";
import { AdminComponent } from "./admin.component";
import { GettingStartedComponent } from "./views/getting-started/getting-started.component";
import { LiquidSolutionsComponent } from "./views/liquid-solutions/liquid-solutions.component";
import { HeaderComponent } from "./includes/header/header.component";
import { SidebarComponent } from "./includes/sidebar/sidebar.component";
import { FooterComponent } from "./includes/footer/footer.component";
import { CommonService } from "../../utils/services/common.service";

import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SignaturePadModule } from 'angular2-signaturepad';
import { HighchartsChartModule } from 'highcharts-angular';

import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';



import { AddFinancialInformationComponent } from "./views/add-financial-information/add-financial-information.component";
import { CompanyViewComponent } from './views/company-view/company-view.component';
import { StepsComponent } from '../../widgets/steps/steps.component';
import { CompanyListComponent } from './views/company-list/company-list.component';
import { NetWorthChartComponent } from './views/net-worth-chart/net-worth-chart.component';
import { ContactComponent } from './views/contact/contact.component';
import { ChartComponent } from '../../widgets/chart/chart.component';
import { NonDisclosureAgreementComponent } from './wizard/non-disclosure-agreement/non-disclosure-agreement.component';
import { DecisionSupportModelComponent } from './wizard/decision-support-model/decision-support-model.component';
import { LiquiditySolutionComponent } from './wizard/liquidity-solution/liquidity-solution.component';
import { LiquidityApplicationComponent } from './wizard/liquidity-application/liquidity-application.component';
import { PrivateAdvanceAgreementComponent } from './wizard/private-advance-agreement/private-advance-agreement.component';
import { ToggleComponent } from '../../widgets/toggle/toggle.component';
import { SignatureComponent } from '../../widgets/signature/signature.component';
import { PrettyPrintPipe } from '../../utils/pretty-print';
import { NumberDirective } from '../../utils/numbers-only';
import { NumberComponent } from '../../widgets/number/number.component';
import { ProfileComponent } from './views/profile/profile.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from 'src/app/utils/shared.module';
import { AdminService } from 'src/app/utils/services/admin.service';
@NgModule({
  declarations: [
    AdminComponent,
    GettingStartedComponent,
    LiquidSolutionsComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    AddFinancialInformationComponent,
    CompanyViewComponent,
    StepsComponent,
    ToggleComponent,
    SignatureComponent,
    CompanyListComponent,
    NetWorthChartComponent,
    ContactComponent,
    ChartComponent,
    NonDisclosureAgreementComponent,
    DecisionSupportModelComponent,
    LiquiditySolutionComponent,
    LiquidityApplicationComponent,
    PrivateAdvanceAgreementComponent,
    PrettyPrintPipe,
    NumberDirective,
    NumberComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule, 
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    PaginationModule.forRoot(),
    HighchartsChartModule,
    NgxDatatableModule,
    SignaturePadModule,
    ReactiveFormsModule],
  providers: [CommonService,AdminService]
})
export class AdminModule {}
