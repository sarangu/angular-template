import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'src/app/utils/services/common.service';
import { animations } from 'src/app/utils/animations';
@Component({
  selector: 'app-liquid-solutions',
  templateUrl: './liquid-solutions.component.html',
  styleUrls: ['./liquid-solutions.component.scss'],
  animations:animations
})
export class LiquidSolutionsComponent implements OnInit {
  title;
  step = "non-disclosure-agreement";
  steps = [
    { step: 'Non-Disclosure <br/>Agreement', slug: 'non-disclosure-agreement' },
    { step: 'Decision Support <br/>Model', slug: 'decision-support-model' },
    { step: 'Liquidity <br/>Solution', slug: 'liquidity-solution' },
    { step: 'Liquidity <br/>Application', slug: 'liquidity-application' },
    { step: 'Private Advance <br/>Agreement', slug: 'private-advance-agreement' },
    { step: 'Confirm <br/>Receipt', slug: 'confirm-receipt' }
  ]
  form = this.fb.group({
    non_disclosure_agreement: this.fb.group({
      signature: ["", Validators.required],
    }),
    decision_support_model: this.fb.group({
      no_of_collateral_shares: ["", [Validators.required,Validators.pattern("^[0-9]*$")]],
      no_of_isos_exercising: ["", [Validators.required,Validators.pattern("^[0-9]*$")]],
      no_of_nsos_exercising: ["", [Validators.required,Validators.pattern("^[0-9]*$")]],
      additional_liquidity_requested: ["", [Validators.required,Validators.pattern("^[0-9]*$")]]
    }),
    liquidity_solution: this.fb.group({
      solution: ["", Validators.required]
    }),
    liquidity_application: this.fb.group({
      background_check: [""],
      marital_status: ["single", Validators.required],
      drivers_license: ["", Validators.required],
      drivers_license2: [""],
      proof_of_residence: ["", Validators.required]
    }),
    private_advance_agreement: this.fb.group({
      approvals: ["", Validators.required],
      signature: ["", Validators.required]
    }),
  })
  constructor(private route: ActivatedRoute, private router: Router, public api: CommonService, private fb: FormBuilder) {
    this.route.params.subscribe(data => {
      this.title = data.company;
    })
  }
  ngOnInit() {

  }
  go(item) {
    this.step = item;
    window.scroll(0,0);
  }
  
}
