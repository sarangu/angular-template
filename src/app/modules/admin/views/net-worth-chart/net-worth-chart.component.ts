import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/utils/services/common.service';
import { animations } from 'src/app/utils/animations';
import { AdminService } from 'src/app/utils/services/admin.service';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-net-worth-chart',
  templateUrl: './net-worth-chart.component.html',
  styleUrls: ['./net-worth-chart.component.scss'],
  animations: animations
})
export class NetWorthChartComponent implements OnInit {
  Highcharts = Highcharts;
  chartConstructor = "chart";
  updateFromInput = false;
  chartOptions = {
    series: [],
    chart: {
      spacing: [40, 40, 40, 40],
      backgroundColor: "#282D3C",
      style: {
        fontFamily: 'D-DIN Condensed Regular',
      }
    },
    title: {
      text: 'Contract Amount vs Partial Sale or Full Sale',
      margin: 0,
      style: {
        textTransform: "uppercase",
        fontWeight: "bold",
        fontSize: "20px",
        color: "#fff"
      }
    },
    yAxis: {
      gridLineColor: 'rgba(255,255,255,.2)',
      lineWidth: 1,
      lineColor: '#fff',
      labels: {
        style: {
          color: '#fff',
          fontSize: "15px"
        }
      },
      title: {
        text: 'Net After-Tax Value at Sale',
        margin: 20,
        style: {
          textTransform: "uppercase",
          fontWeight: "bold",
          fontSize: "16px",
          color: "#fff"
        }
      }
    },
    xAxis: {
      gridLineColor: 'rgba(255,255,255,.3)',
      lineColor: '#fff',
      labels: {
        style: {
          color: '#fff',
          fontSize: "15px"
        }
      },
      title: {
        text: 'Projected Price Per Share at Exit',
        margin: 20,
        style: {
          textTransform: "uppercase",
          fontWeight: "bold",
          fontSize: "16px",
          color: "#fff"
        }
      },
      categories: [5.00, 7.50, 10.00, 12.50, 15.00, 17.50, 20.00, 22.50, 25.00, 27.50, 30.00]
    },
    legend: {
      verticalAlign: "top",
      margin: 40,
      itemStyle: {
        color: '#fff'
      }
    }
  };
  submitted = false;
  item;
  item2;
  chartData;
  values;
  form = this.fb.group({
    current_market_value: ['', Validators.required],
    repayment_date: ['', Validators.required],
    funding_date: ['', Validators.required],
    liquidity: ['', Validators.required],
    current409AValuation: ['', Validators.required],
    lastRoundValuation: ['', Validators.required],
    fullyDilutedShareCount: ['', Validators.required],
    collateralShares: ['', Validators.required],
    estCostBasisofShares: ['', Validators.required],
  })
  stats = {
    funding:0,
    selling_shares:0,
    full_sale:0
  }
  view = '1';
  constructor(private fb: FormBuilder, private route: ActivatedRoute, public common: CommonService, private admin: AdminService, private cd: ChangeDetectorRef) {
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.admin.liquidSolutionsSingle(params.id).subscribe(res => {
        this.item = res;
        this.route.queryParams.subscribe(queryParams => {
          this.item2 = queryParams;
          this.form.patchValue(this.item);
          this.form.patchValue(this.item2);
          this.chart(params.id, this.form)
          this.form.valueChanges.subscribe(data=>{
            this.chart(params.id, this.form)
          })
        })
      })
    });
    this.values = this.admin.values;
  }
  changeView(item) {
    this.view = item;
  }
  chart(id, data) {
    this.admin.liquidSolutionsChart(id, {
      liquidity: parseInt(data.value.liquidity),
      current_market_value: parseInt(data.value.current_market_value),
      funding_date: data.value.funding_date,
      repayment_date: data.value.repayment_date
    }).subscribe(res => {
      this.chartData = res;
      setTimeout(() => {

      // let index = this.values.index(data.value.current_market_value);
      // console.log(index)

      this.values.forEach((item,i) => {
        if(item == data.value.current_market_value){
         
          this.stats.funding = this.chartData['funding'][i];
          this.stats.selling_shares = this.chartData['selling_shares'][i];
          this.stats.full_sale = this.chartData['full_sale'][i];
        }
        
      });


        // let index = this.chartData['funding']


        this.chartOptions.series = [
          {
            name: "Contract Amount",
            color: '#3498db',
            marker: {
              enabled: false,
              symbol: "circle"
            },
            data: this.chartData['funding']
            // data: [324226.8041,	324226.8041,	324226.8041,	324226.8041,	324226.8041,	324226.8041,	324226.8041,	324226.8041, 324226.8041,	324226.8041,	324226.8041]
          },
          {
            name: "Partial Sale",
            color: '#e74c3c',
            marker: {
              enabled: false,
              symbol: "circle"
            },
            data: this.chartData['selling_shares']
            // data: [258903.0383,	191726.9058,	88969.20778,	-13788.49028,	-116546.1883,	-219303.8864,	-322061.5844,	-424819.2825,	-527576.9806,	-630334.6786,	-733092.3767]
          },
          {
            name: "Full Sale",
            color: '#2ecc71',
            marker: {
              enabled: false,
              symbol: "circle"
            },
            data: this.chartData['full_sale']
            // data: [475.97,	475.97,	475.97,	475.97,	475.97,	475.97,	475.97,	475.97,	475.97,	475.97,	475.97]
          }
        ]
        this.updateFromInput = true;
      }, 0);
    })
  }
}
