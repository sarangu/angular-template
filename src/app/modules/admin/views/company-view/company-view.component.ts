import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { CommonService } from "../../../../utils/services/common.service";
import { FormBuilder,Validators } from '@angular/forms';
import { AdminService } from 'src/app/utils/services/admin.service';




@Component({
  selector: 'app-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.scss'],
})
export class CompanyViewComponent implements OnInit {
  submitted = false;
  title = '';
  values;
  form = this.fb.group({
    current_market_value:['',Validators.required],
    repayment_date:['',Validators.required],
    funding_date:['',Validators.required],
    liquidity:['',Validators.required]

  })
  item;
  bsConfig;
  constructor(private route:ActivatedRoute,private router:Router,public common:CommonService,private admin :AdminService,private fb:FormBuilder) {
    this.route.params.subscribe(data=>{
      this.admin.liquidSolutionsSingle(data.id).subscribe(res => {
      
        this.item = res;
      
      })
   
    })
  }
  ngOnInit() {
    this.bsConfig = this.common.datePicker.config;
    this.values = this.admin.values;

  }

  go(e){
    e.preventDefault();
   
if(this.form.status == 'VALID'){
 
  this.router.navigate(['admin/net-worth-chart/'+this.item._id],{queryParams:this.form.value})
 
}else{
  this.submitted = true;
}
  }


}
