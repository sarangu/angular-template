import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/utils/services/common.service';
import { animations } from 'src/app/utils/animations';
import { AdminService } from 'src/app/utils/services/admin.service';
@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss'],
  animations: animations
})
export class CompanyListComponent implements OnInit, AfterViewInit {
  @ViewChild('actions', { static: false }) actions: TemplateRef<any>;
  @ViewChild('badge', { static: false }) badge: TemplateRef<any>;
  columns = []
  temp: any = [];
  companies: any = [];
  page = 1;
  constructor(private cd: ChangeDetectorRef, public common: CommonService, private admin: AdminService, private router: Router) { }
  ngOnInit() { 
    this.read();
  }
  ngAfterViewInit() {
    this.columns = [
      { prop: 'companyName', name: "Company Name", cellTemplate: this.badge },
      { prop: 'state', name: "State" },
      { prop: 'actions', name: "Actions", cellTemplate: this.actions, headerClass: "text-right", cellClass: "text-right", sortable: false }
    ];
    this.cd.detectChanges();
  }
  read() {
    this.admin.liquidSolutionsAll().subscribe(res => {
      this.temp = res;
      this.companies = res['reverse']()
    })
  }
  
  delete(item) {
    this.common.alert.fire({
      title: 'Are you sure?',
      text: "You won't to delete " + item.companyName,
      icon: 'warning',
      showCancelButton: true,
    }).then((result) => {
      if (result.value) {
        this.admin.liquidSolutionsDelete(item._id).subscribe(data => {
          this.common.toster("Liquid Solution Deleted Successfully", "Done!", "success");          
          this.common.sidebarEmmiterEmit();
          this.read();
          
      
        })
      }
    })
  }
  search(e) {
    this.companies = e;
  }
  edit(item) {
    this.router.navigate(["admin/financial-information", item._id])
   
    console.log(item)
  }
}
