import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormArray, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AdminService } from 'src/app/utils/services/admin.service';
import { CommonService } from 'src/app/utils/services/common.service';
@Component({
  selector: "app-add-financial-information",
  templateUrl: "./add-financial-information.component.html",
  styleUrls: ["./add-financial-information.component.scss"],
})
export class AddFinancialInformationComponent implements OnInit {
  title = '';
  submitted = false;
  companies: any;
  states = [];
  iso = new FormControl(false);
  nso = new FormControl(false);
  edit = false;
  editData;
  form = this.fb.group({
    companyName: ["", [Validators.required, Validators.pattern(this.common.patterns.alphabets)]],
    ownedShares: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    pricePaid: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    valuation409AWhenExercised: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    numberOfISO: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    state: ["", [Validators.required, Validators.pattern(this.common.patterns.alphabets)]],
    ISO: this.fb.array([]),
    numberOfNSO: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    NSO: this.fb.array([]),
    current409AValuation: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    lastRoundValuation: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    fullyDilutedShareCount: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
    additionalLiquidity: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]]
  })
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, public admin: AdminService, public common: CommonService) {
    this.route.params.subscribe(res => {
      if (res.company) {
        this.admin.liquidSolutionsSingle(res.company).subscribe(res => {
          this.title = res['companyName'];
          this.edit = true;
          this.editData = res;
          if (this.editData['ISO'].length > 0) {
            this.iso.setValue(true);
          }
          if (this.editData['ISO'].length > 0) {
            this.nso.setValue(true);
          }
          this.form.patchValue(res)
        })
      }
    })
  }
  ngOnInit() {
    this.states = this.common.states;
    this.iso.valueChanges.subscribe(data => {
      if (data === true) {
        if (this.edit) {
          if (this.editData['ISO'].length > 0) {
            for (let i = 0; i < this.editData['ISO'].length; i++) {
              (this.form.get('ISO') as FormArray).push(this.grant());
            }
            this.form.get("ISO").patchValue(this.editData['ISO'])
          } else {
            (this.form.get('ISO') as FormArray).push(this.grant());
          }
        } else {
          (this.form.get('ISO') as FormArray).push(this.grant());
        }
      } else {
        (this.form.get('ISO') as FormArray).clear();
      }
    });
    this.nso.valueChanges.subscribe(data => {
      if (data === true) {
        if (this.edit) {
          if (this.editData['ISO'].length > 0) {
            for (let i = 0; i < this.editData['NSO'].length; i++) {
              (this.form.get('NSO') as FormArray).push(this.grant());
            }
            this.form.get("NSO").patchValue(this.editData['NSO'])
          } else {
            (this.form.get('NSO') as FormArray).push(this.grant());
          }
        } else {
          (this.form.get('NSO') as FormArray).push(this.grant());
        }
      } else {
        (this.form.get('NSO') as FormArray).clear();
      }
    });
    this.admin.companiesAll().subscribe(data => {
      this.companies = data;
    });
  }
  /**
* @function 
* @name grant  
* @desc to build new empty grant form builder group
* @returns form builder group object
*/
  grant() {
    let obj = this.fb.group({
      grantDate: ["", [Validators.required]],
      strikePrice: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]],
      noOfStrikesGranted: ["", [Validators.required, Validators.pattern(this.common.patterns.numberDecimal), Validators.min(1)]]
    });
    return obj;
  }
  /**
* @function 
* @name addmore  
* @desc to appending new grant row under ISO/NSO
* @param item  Where do you want to add grant to "iso" or "nso"
*/
  addmore(item) {
    if (item === "iso") {
      (this.form.get('ISO') as FormArray).push(this.grant());
    }
    if (item === "nso") {
      (this.form.get('NSO') as FormArray).push(this.grant());
    }
  }
  /**
  * @function 
  * @name remove
  * @desc to remove selected grant row from ISO / NSO
  * @param i Index of which item you wish to remove
  * @param val  Where do you want to remove grant from "iso" or "nso"
  */
  remove(i, val) {
    if (val === 'iso') {
      (this.form.get('ISO') as FormArray).removeAt(i);
    }
    if (val === 'nso') {
      (this.form.get('NSO') as FormArray).removeAt(i);
    }
  }
  /**
   * @function 
   * @name stringConvert
   * @desc to convert all numeric string values to integers before submit/update  financial information
   */
  stringConvert() {
    this.form.get("ownedShares").setValue(parseInt(this.form.get("ownedShares").value));
    this.form.get("pricePaid").setValue(parseInt(this.form.get("pricePaid").value));
    this.form.get("valuation409AWhenExercised").setValue(parseInt(this.form.get("valuation409AWhenExercised").value));
    this.form.get("numberOfISO").setValue(parseInt(this.form.get("numberOfISO").value));
    this.form.get("numberOfNSO").setValue(parseInt(this.form.get("numberOfISO").value));
    this.form.get("current409AValuation").setValue(parseInt(this.form.get("current409AValuation").value));
    this.form.get("lastRoundValuation").setValue(parseInt(this.form.get("lastRoundValuation").value));
    this.form.get("fullyDilutedShareCount").setValue(parseInt(this.form.get("fullyDilutedShareCount").value));
    this.form.get("additionalLiquidity").setValue(parseInt(this.form.get("additionalLiquidity").value));
  }
  /**
  * @function 
  * @name submit 
  * @desc to submit new financial information 
  */
  submit() {
    this.submitted = true;
    if (this.form.valid) {
      this.stringConvert();
      this.admin.liquidSolutionsAdd(this.form.value).subscribe(data => {
        this.common.sidebarEmmiterEmit();
        this.router.navigate(['admin']);
        this.common.toster("Financial Information Added Successfully", "Done!", "success");
      })
    } else {
      window.scrollTo(0, 0);
    }
  }
  /**
   * @function 
   * @name update
   * @desc to update existing financial information 
   */
  update() {
    this.submitted = true;
    if (this.form.valid) {
      this.stringConvert();
      this.admin.liquidSolutionsUpdate(this.editData._id, this.form.value).subscribe(data => {
        this.common.sidebarEmmiterEmit();
        this.router.navigate(['admin']);
        this.common.toster("Financial Information Updated Successfully", "Done!", "success");
      })
    } else {
      window.scrollTo(0, 0);
    }
  }
  /**
   * @function 
   * @name onSelect
   * @desc receive selected company object and patch to form builder
   * @params event
   */
  onSelect(event) {
    this.form.get("current409AValuation").setValue(event.item.current409AValuation)
    this.form.get("lastRoundValuation").setValue(event.item.lastRoundValuation)
    this.form.get("fullyDilutedShareCount").setValue(event.item.fullyDilutedShareCount)
  }
}
