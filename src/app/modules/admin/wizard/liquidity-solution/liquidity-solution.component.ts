import { Component, OnInit, EventEmitter, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ControlContainer } from '@angular/forms';
@Component({
  selector: 'app-liquidity-solution',
  templateUrl: './liquidity-solution.component.html',
  styleUrls: ['./liquidity-solution.component.scss']
})
export class LiquiditySolutionComponent implements OnInit {
  @Output() step = new EventEmitter();
  modalRef: BsModalRef;
  submitted =  false;
  constructor(private modalService: BsModalService, public cc: ControlContainer) { }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>) {
   if(this.cc.control.status == 'VALID'){
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-dialog-centered',animated:true }));
   }else{
    this.submitted =  true;
   }
  }

  previous(item) {
    this.step.emit(item)
  }
  next(item) {
    this.step.emit(item)
    this.modalRef.hide()
  }
}
