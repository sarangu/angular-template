import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquiditySolutionComponent } from './liquidity-solution.component';

describe('LiquiditySolutionComponent', () => {
  let component: LiquiditySolutionComponent;
  let fixture: ComponentFixture<LiquiditySolutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquiditySolutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquiditySolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
