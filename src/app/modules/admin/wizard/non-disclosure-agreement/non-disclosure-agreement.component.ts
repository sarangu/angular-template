import { Component, OnInit, ViewChild,Output,EventEmitter } from '@angular/core';
import { ControlContainer, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-non-disclosure-agreement',
  templateUrl: './non-disclosure-agreement.component.html',
  styleUrls: ['./non-disclosure-agreement.component.scss']
})
export class NonDisclosureAgreementComponent implements OnInit {
  @Output() step = new EventEmitter();
  submitted = false;
  constructor(public cc: ControlContainer) { }

  ngOnInit() {
  }
 
  next(item){
  
    if(this.cc.control.status == 'VALID'){
      this.step.emit(item);
    }else{
      this.submitted  = true;
    }

  }

}
