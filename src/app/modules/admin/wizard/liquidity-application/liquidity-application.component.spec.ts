import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidityApplicationComponent } from './liquidity-application.component';

describe('LiquidityApplicationComponent', () => {
  let component: LiquidityApplicationComponent;
  let fixture: ComponentFixture<LiquidityApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidityApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidityApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
