import { Component, OnInit, EventEmitter,Output, TemplateRef } from '@angular/core';
import { ControlContainer,Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-liquidity-application',
  templateUrl: './liquidity-application.component.html',
  styleUrls: ['./liquidity-application.component.scss']
})
export class LiquidityApplicationComponent implements OnInit {
  @Output() step = new EventEmitter();
  modalRef: BsModalRef;
  finalize = false;
  submitted = false;
  constructor(public cc:ControlContainer,private modalService: BsModalService) { }

  ngOnInit() {

    this.cc.control.get('marital_status').valueChanges.subscribe(data=>{
      console.log(data);
      if(data == 'married'){
        this.cc.control.get('drivers_license2').setValidators(Validators.required);
      }else{
        
        this.cc.control.get('drivers_license2').clearValidators();
      }
      
    })
  }
  openModal(template: TemplateRef<any>) {
    if(this.cc.control.status == 'VALID'){
      this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-dialog-centered',animated:true }));
    }else{
      this.submitted = true;
    }
   
  }
  finalized(){
    this.finalize = true;
  }
  previous(item){
    this.step.emit(item);
  }
  next(item){
    this.step.emit(item);
    this.modalRef.hide()
  }


}
