import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateAdvanceAgreementComponent } from './private-advance-agreement.component';

describe('PrivateAdvanceAgreementComponent', () => {
  let component: PrivateAdvanceAgreementComponent;
  let fixture: ComponentFixture<PrivateAdvanceAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateAdvanceAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateAdvanceAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
