import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { ControlContainer } from '@angular/forms';
@Component({
  selector: 'app-private-advance-agreement',
  templateUrl: './private-advance-agreement.component.html',
  styleUrls: ['./private-advance-agreement.component.scss']
})
export class PrivateAdvanceAgreementComponent implements OnInit {
  @Output() step = new EventEmitter();
  submitted = false;
  constructor(public cc: ControlContainer) { }

  ngOnInit() {
  }
  previous(item) {
    this.step.emit(item)
  }
  next(item){
    if(this.cc.control.status == 'VALID'){
      this.step.emit(item);
    }else{
      this.submitted = true;
    }

  }

}
