import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecisionSupportModelComponent } from './decision-support-model.component';

describe('DecisionSupportModelComponent', () => {
  let component: DecisionSupportModelComponent;
  let fixture: ComponentFixture<DecisionSupportModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecisionSupportModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecisionSupportModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
