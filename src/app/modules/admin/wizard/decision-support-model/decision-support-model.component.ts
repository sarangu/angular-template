import { Component, OnInit, EventEmitter,Output } from '@angular/core';
import { ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-decision-support-model',
  templateUrl: './decision-support-model.component.html',
  styleUrls: ['./decision-support-model.component.scss']
})
export class DecisionSupportModelComponent implements OnInit {
@Output() step = new EventEmitter();
submitted = false;
  constructor(public cc:ControlContainer) { }

  ngOnInit() {
  }

  previous(item){
    this.step.emit(item);
  }
  next(item){

    if(this.cc.control.status == 'VALID'){
      this.step.emit(item);
    }else{
     this.submitted = true;
    }
  }

}
