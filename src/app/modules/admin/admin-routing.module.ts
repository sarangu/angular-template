import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { GettingStartedComponent } from './views/getting-started/getting-started.component';
import { LiquidSolutionsComponent } from './views/liquid-solutions/liquid-solutions.component';
import { AddFinancialInformationComponent } from './views/add-financial-information/add-financial-information.component';
import { CompanyViewComponent } from './views/company-view/company-view.component';
import { CompanyListComponent } from './views/company-list/company-list.component';
import { NetWorthChartComponent } from './views/net-worth-chart/net-worth-chart.component';
import { ContactComponent } from './views/contact/contact.component';
import { ProfileComponent } from './views/profile/profile.component';

const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      {
        path:"getting-started",
        component:GettingStartedComponent
      },{
        path:"learn-more",
        component:ContactComponent,

      },{
        path:"",
        component:CompanyListComponent
      },{
        path:"company/:id",
        component:CompanyViewComponent
      },{
        path:"financial-information",
        component:AddFinancialInformationComponent
      },{
        path:"financial-information/:company",
        component:AddFinancialInformationComponent
      }, {
        path:":company/liquid-solutions",
        component:LiquidSolutionsComponent
      },{
        path:"net-worth-chart/:id",
        component:NetWorthChartComponent
      },{
        path:"profile",
        component:ProfileComponent
      }

      // ,{
      //   path:'',
      //   redirectTo:'/admin/getting-started',
      //   pathMatch:'full'
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
