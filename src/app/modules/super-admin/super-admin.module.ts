import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { SuperAdminComponent } from './super-admin.component';
import { SuperAdminRoutingModule } from "./super-admin-routing.module";
import { HeaderComponent } from "./includes/header/header.component";
import { SidebarComponent } from "./includes/sidebar/sidebar.component";
import { FooterComponent } from "./includes/footer/footer.component";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UsersComponent } from './views/users/users.component';
import { AdvisorsComponent } from './views/advisors/advisors.component';
import { CompaniesComponent } from './views/companies/companies.component';
import { TaxesComponent } from './views/taxes/taxes.component';
import { SettingsComponent } from './views/settings/settings.component';
import { LiquidSolutionsComponent } from './views/liquid-solutions/liquid-solutions.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from 'src/app/utils/shared.module';
import { CommonService } from 'src/app/utils/services/common.service';
import { SuperAdminService } from 'src/app/utils/services/super-admin.service';

@NgModule({
  declarations: [
    DashboardComponent,
    SuperAdminComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    UsersComponent,
    AdvisorsComponent,
    CompaniesComponent,
    TaxesComponent,
    SettingsComponent,
    LiquidSolutionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    SuperAdminRoutingModule,
    NgxDatatableModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers:[CommonService,SuperAdminService]
})
export class SuperAdminModule { }
