import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { SuperAdminComponent } from './super-admin.component';
import { UsersComponent } from './views/users/users.component';
import { AdvisorsComponent } from './views/advisors/advisors.component';
import { CompaniesComponent } from './views/companies/companies.component';
import { TaxesComponent } from './views/taxes/taxes.component';
import { SettingsComponent } from './views/settings/settings.component';
import { LiquidSolutionsComponent } from './views/liquid-solutions/liquid-solutions.component';

const routes: Routes = [
  {
    path:'',
    component:SuperAdminComponent,
    children:[
      {
        path:"",
        component:DashboardComponent
      },
      {
        path:"users",
        component:UsersComponent
      },
      {
        path:"advisors",
        component:AdvisorsComponent
      },
      {
        path:"companies",
        component:CompaniesComponent
      },
      {
        path:"taxes",
        component:TaxesComponent
      },
      {
        path:"settings",
        component:SettingsComponent
      },
      {
        path:"liquid-solutions",
        component:LiquidSolutionsComponent
      }

    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperAdminRoutingModule { }
