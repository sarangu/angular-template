import { Component, OnInit } from '@angular/core';
import { animations } from 'src/app/utils/animations';

@Component({
  selector: 'app-super-admin',
  templateUrl: './super-admin.component.html',
  styleUrls: ['./super-admin.component.scss'],
  animations:animations
})
export class SuperAdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
