import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidSolutionsComponent } from './liquid-solutions.component';

describe('LiquidSolutionsComponent', () => {
  let component: LiquidSolutionsComponent;
  let fixture: ComponentFixture<LiquidSolutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidSolutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidSolutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
