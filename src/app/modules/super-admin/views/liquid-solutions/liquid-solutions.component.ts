import { Component, OnInit,TemplateRef,ViewChild,AfterViewInit,ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-liquid-solutions',
  templateUrl: './liquid-solutions.component.html',
  styleUrls: ['./liquid-solutions.component.scss']
})
export class LiquidSolutionsComponent implements OnInit,AfterViewInit{

  @ViewChild('actions1',{static:false}) actions1: TemplateRef<any>;
  @ViewChild('status1',{static:false}) status1: TemplateRef<any>;
  @ViewChild('actions2',{static:false}) actions2: TemplateRef<any>;
  @ViewChild('status2',{static:false}) status2: TemplateRef<any>;
  viewMode = 'tab1';
  rows1 = [];
  columns1 = [];
  rows2 = [];
  columns2 = [];

  constructor(private cd:ChangeDetectorRef) { }
  switchview(tab){
    this.viewMode = tab;
  setTimeout(() => {

    this.columns2 = [
      { prop: 'user',name:"User" },
      { prop: 'company',name:"Company" },
      { prop: 'status2',name:"Status",cellTemplate: this.status2},
      { prop: 'actions2',name:"Actions",cellTemplate: this.actions2,headerClass: "text-right",cellClass:"text-right",sortable:false }

    ];

  }, 100);

  }
  ngAfterViewInit(){
    console.log("ngAfterViewInit")

    this.columns1 = [
      { prop: 'user',name:"User" },
      { prop: 'company',name:"Company" },
      { prop: 'status1',name:"Status",cellTemplate: this.status1},
      { prop: 'actions1',name:"Actions",cellTemplate: this.actions1,headerClass: "text-right",cellClass:"text-right",sortable:false }
    ];

    this.rows1 = [{
      "id": 1,
      "user": "jblown0",
      "company": "Leenti"
    }, {
      "id": 2,
      "user": "aeverington1",
      "company": "Jabbercube"
    }, {
      "id": 3,
      "user": "ssatchel2",
      "company": "Linkbridge"
    }, {
      "id": 4,
      "user": "bdugget3",
      "company": "Oyoyo"
    }, {
      "id": 5,
      "user": "cmarple4",
      "company": "Zoomlounge"
    }, {
      "id": 6,
      "user": "glepere5",
      "company": "Eayo"
    }, {
      "id": 7,
      "user": "rgristhwaite6",
      "company": "Bubbletube"
    }, {
      "id": 8,
      "user": "mcrannell7",
      "company": "Youspan"
    }, {
      "id": 9,
      "user": "jambroise8",
      "company": "Zazio"
    }, {
      "id": 10,
      "user": "sanan9",
      "company": "Ntag"
    }, {
      "id": 11,
      "user": "ljurischa",
      "company": "Muxo"
    }, {
      "id": 12,
      "user": "nveckb",
      "company": "Topdrive"
    }, {
      "id": 13,
      "user": "acogginc",
      "company": "Skibox"
    }, {
      "id": 14,
      "user": "rtrevanced",
      "company": "Shuffledrive"
    }, {
      "id": 15,
      "user": "rcowoppee",
      "company": "Oyoyo"
    }, {
      "id": 16,
      "user": "rgrubeyf",
      "company": "Thoughtblab"
    }, {
      "id": 17,
      "user": "scoatsworthg",
      "company": "Skidoo"
    }, {
      "id": 18,
      "user": "nkobpah",
      "company": "Twitterbridge"
    }, {
      "id": 19,
      "user": "ppallisi",
      "company": "Shufflebeat"
    }, {
      "id": 20,
      "user": "sdirandj",
      "company": "Babbleset"
    }, {
      "id": 21,
      "user": "gstoadek",
      "company": "Tagfeed"
    }, {
      "id": 22,
      "user": "njanczykl",
      "company": "Muxo"
    }, {
      "id": 23,
      "user": "rdubsm",
      "company": "Myworks"
    }, {
      "id": 24,
      "user": "ejacquemetn",
      "company": "Mudo"
    }, {
      "id": 25,
      "user": "rdumpletono",
      "company": "Eidel"
    }, {
      "id": 26,
      "user": "jstampep",
      "company": "Edgeclub"
    }, {
      "id": 27,
      "user": "ltwitteyq",
      "company": "Oyonder"
    }, {
      "id": 28,
      "user": "jcoaler",
      "company": "Tazz"
    }, {
      "id": 29,
      "user": "icouzenss",
      "company": "Skidoo"
    }, {
      "id": 30,
      "user": "cvalancet",
      "company": "Youspan"
    }, {
      "id": 31,
      "user": "ghaggusu",
      "company": "Cogibox"
    }, {
      "id": 32,
      "user": "bshallcrassv",
      "company": "Jabberstorm"
    }, {
      "id": 33,
      "user": "tloffheadw",
      "company": "Oodoo"
    }, {
      "id": 34,
      "user": "gyeardleyx",
      "company": "Flashdog"
    }, {
      "id": 35,
      "user": "tdewitty",
      "company": "Centizu"
    }, {
      "id": 36,
      "user": "tgirauxz",
      "company": "Bubbletube"
    }, {
      "id": 37,
      "user": "jwisker10",
      "company": "Avamm"
    }, {
      "id": 38,
      "user": "mmarsay11",
      "company": "Leenti"
    }, {
      "id": 39,
      "user": "wdensham12",
      "company": "Centimia"
    }, {
      "id": 40,
      "user": "gdecker13",
      "company": "Wordify"
    }, {
      "id": 41,
      "user": "wtuison14",
      "company": "Jabberbean"
    }, {
      "id": 42,
      "user": "kswettenham15",
      "company": "Kaymbo"
    }, {
      "id": 43,
      "user": "eaxup16",
      "company": "Meeveo"
    }, {
      "id": 44,
      "user": "nkorf17",
      "company": "Topiczoom"
    }, {
      "id": 45,
      "user": "mfrangello18",
      "company": "Dazzlesphere"
    }, {
      "id": 46,
      "user": "korae19",
      "company": "Wikizz"
    }, {
      "id": 47,
      "user": "jlamboll1a",
      "company": "Wordtune"
    }, {
      "id": 48,
      "user": "aasaaf1b",
      "company": "Yabox"
    }, {
      "id": 49,
      "user": "gtomowicz1c",
      "company": "Fivespan"
    }, {
      "id": 50,
      "user": "aduesbury1d",
      "company": "Kaymbo"
    }]



    this.rows2 = [{
      "id": 1,
      "user": "jblown0",
      "company": "Leenti"
    }, {
      "id": 2,
      "user": "aeverington1",
      "company": "Jabbercube"
    }, {
      "id": 3,
      "user": "ssatchel2",
      "company": "Linkbridge"
    }, {
      "id": 4,
      "user": "bdugget3",
      "company": "Oyoyo"
    }, {
      "id": 5,
      "user": "cmarple4",
      "company": "Zoomlounge"
    }, {
      "id": 6,
      "user": "glepere5",
      "company": "Eayo"
    }, {
      "id": 7,
      "user": "rgristhwaite6",
      "company": "Bubbletube"
    }, {
      "id": 8,
      "user": "mcrannell7",
      "company": "Youspan"
    }, {
      "id": 9,
      "user": "jambroise8",
      "company": "Zazio"
    }, {
      "id": 10,
      "user": "sanan9",
      "company": "Ntag"
    }, {
      "id": 11,
      "user": "ljurischa",
      "company": "Muxo"
    }, {
      "id": 12,
      "user": "nveckb",
      "company": "Topdrive"
    }, {
      "id": 13,
      "user": "acogginc",
      "company": "Skibox"
    }, {
      "id": 14,
      "user": "rtrevanced",
      "company": "Shuffledrive"
    }, {
      "id": 15,
      "user": "rcowoppee",
      "company": "Oyoyo"
    }, {
      "id": 16,
      "user": "rgrubeyf",
      "company": "Thoughtblab"
    }, {
      "id": 17,
      "user": "scoatsworthg",
      "company": "Skidoo"
    }, {
      "id": 18,
      "user": "nkobpah",
      "company": "Twitterbridge"
    }, {
      "id": 19,
      "user": "ppallisi",
      "company": "Shufflebeat"
    }, {
      "id": 20,
      "user": "sdirandj",
      "company": "Babbleset"
    }, {
      "id": 21,
      "user": "gstoadek",
      "company": "Tagfeed"
    }, {
      "id": 22,
      "user": "njanczykl",
      "company": "Muxo"
    }, {
      "id": 23,
      "user": "rdubsm",
      "company": "Myworks"
    }, {
      "id": 24,
      "user": "ejacquemetn",
      "company": "Mudo"
    }, {
      "id": 25,
      "user": "rdumpletono",
      "company": "Eidel"
    }, {
      "id": 26,
      "user": "jstampep",
      "company": "Edgeclub"
    }, {
      "id": 27,
      "user": "ltwitteyq",
      "company": "Oyonder"
    }, {
      "id": 28,
      "user": "jcoaler",
      "company": "Tazz"
    }, {
      "id": 29,
      "user": "icouzenss",
      "company": "Skidoo"
    }, {
      "id": 30,
      "user": "cvalancet",
      "company": "Youspan"
    }, {
      "id": 31,
      "user": "ghaggusu",
      "company": "Cogibox"
    }, {
      "id": 32,
      "user": "bshallcrassv",
      "company": "Jabberstorm"
    }, {
      "id": 33,
      "user": "tloffheadw",
      "company": "Oodoo"
    }, {
      "id": 34,
      "user": "gyeardleyx",
      "company": "Flashdog"
    }, {
      "id": 35,
      "user": "tdewitty",
      "company": "Centizu"
    }, {
      "id": 36,
      "user": "tgirauxz",
      "company": "Bubbletube"
    }, {
      "id": 37,
      "user": "jwisker10",
      "company": "Avamm"
    }, {
      "id": 38,
      "user": "mmarsay11",
      "company": "Leenti"
    }, {
      "id": 39,
      "user": "wdensham12",
      "company": "Centimia"
    }, {
      "id": 40,
      "user": "gdecker13",
      "company": "Wordify"
    }, {
      "id": 41,
      "user": "wtuison14",
      "company": "Jabberbean"
    }, {
      "id": 42,
      "user": "kswettenham15",
      "company": "Kaymbo"
    }, {
      "id": 43,
      "user": "eaxup16",
      "company": "Meeveo"
    }, {
      "id": 44,
      "user": "nkorf17",
      "company": "Topiczoom"
    }, {
      "id": 45,
      "user": "mfrangello18",
      "company": "Dazzlesphere"
    }, {
      "id": 46,
      "user": "korae19",
      "company": "Wikizz"
    }, {
      "id": 47,
      "user": "jlamboll1a",
      "company": "Wordtune"
    }, {
      "id": 48,
      "user": "aasaaf1b",
      "company": "Yabox"
    }, {
      "id": 49,
      "user": "gtomowicz1c",
      "company": "Fivespan"
    }, {
      "id": 50,
      "user": "aduesbury1d",
      "company": "Kaymbo"
    }]

    this.cd.detectChanges();
    console.log(this.columns1)
  }

  ngOnInit() {
    console.log("ngOnInit")

  }
  dele(item){
    console.log(item)

  }


}


