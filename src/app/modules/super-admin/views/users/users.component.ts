import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { CommonService } from 'src/app/utils/services/common.service';
import { SuperAdminService } from 'src/app/utils/services/super-admin.service';
@Component({
  selector: 'users-dashboard',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {
  @ViewChild('actions', { static: false }) actions: TemplateRef<any>;
  rows: any;
  columns = []
  constructor(private cd: ChangeDetectorRef, private common: CommonService, private superadmin: SuperAdminService) { }
  getUsers() {
    this.superadmin.usersAll().subscribe(res => {
      this.rows = res;
      console.log(res)
    })
  }
  ngAfterViewInit() {
    console.log("ngAfterViewInit")
    this.columns = [
      { prop: 'firstName', name: "First Name" },
      { prop: 'lastName', name: "Last Name" },
      { prop: 'mobile', name: "Mobile" },
      { prop: 'email', name: "Email" },
      { prop: 'companyName', name: "Company Name" },
      { prop: 'actions', name: "Actions", cellTemplate: this.actions, headerClass: "text-right", cellClass: "text-right", sortable: false }
    ];
    this.cd.detectChanges();
  }
  ngOnInit() {
    this.getUsers();
  }
  dele(item) {
    console.log(item)
  }
}
