import { Component, OnInit,TemplateRef,ViewChild,AfterViewInit,ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'advisors-dashboard',
  templateUrl: './advisors.component.html',
  styleUrls: ['./advisors.component.scss']
})
export class AdvisorsComponent implements OnInit,AfterViewInit {
  @ViewChild('actions',{static:false}) actions: TemplateRef<any>;
  rows = [];

  columns = []

  constructor(private cd:ChangeDetectorRef) { }
  ngAfterViewInit(){
    console.log("ngAfterViewInit")

    this.columns = [
      { prop: 'name',name:"Name" },
      { prop: 'email',name:"Email" },
      { prop: 'mobile',name:"Mobile" },
      { prop: 'company',name:"Company" },
      { prop: 'actions',name:"Actions",cellTemplate: this.actions,headerClass: "text-right",cellClass:"text-right",sortable:false }
    ];

    this.rows = [{
      "id": 1,
      "name": "Florina Burwell",
      "email": "fburwell0@pbs.org",
      "mobile": "826-533-2858",
      "company": "Katz"
    }, {
      "id": 2,
      "name": "Corena Stone",
      "email": "cstone1@columbia.edu",
      "mobile": "951-161-1802",
      "company": "Talane"
    }, {
      "id": 3,
      "name": "Zonnya Swetland",
      "email": "zswetland2@wired.com",
      "mobile": "437-313-8076",
      "company": "Yakijo"
    }, {
      "id": 4,
      "name": "Catharine Kienlein",
      "email": "ckienlein3@ameblo.jp",
      "mobile": "741-330-2142",
      "company": "Browsebug"
    }, {
      "id": 5,
      "name": "Bendix Gilby",
      "email": "bgilby4@cyberchimps.com",
      "mobile": "839-194-4715",
      "company": "Ooba"
    }, {
      "id": 6,
      "name": "Lottie Dougan",
      "email": "ldougan5@bbc.co.uk",
      "mobile": "267-690-8510",
      "company": "Digitube"
    }, {
      "id": 7,
      "name": "Meade Roundtree",
      "email": "mroundtree6@com.com",
      "mobile": "385-382-3228",
      "company": "Wordtune"
    }, {
      "id": 8,
      "name": "Cherilyn Hundy",
      "email": "chundy7@clickbank.net",
      "mobile": "277-814-8436",
      "company": "Feedbug"
    }, {
      "id": 9,
      "name": "Nady Bierling",
      "email": "nbierling8@blog.com",
      "mobile": "107-739-5159",
      "company": "Skipfire"
    }, {
      "id": 10,
      "name": "Reina Sang",
      "email": "rsang9@blogtalkradio.com",
      "mobile": "566-590-2608",
      "company": "Tagopia"
    }, {
      "id": 11,
      "name": "Malanie Claus",
      "email": "mclausa@elegantthemes.com",
      "mobile": "645-482-4162",
      "company": "Linktype"
    }, {
      "id": 12,
      "name": "Kordula Sommerland",
      "email": "ksommerlandb@independent.co.uk",
      "mobile": "178-585-3894",
      "company": "Ooba"
    }, {
      "id": 13,
      "name": "Jojo Druce",
      "email": "jdrucec@networksolutions.com",
      "mobile": "102-109-6085",
      "company": "Voomm"
    }, {
      "id": 14,
      "name": "Bowie Jerman",
      "email": "bjermand@ebay.com",
      "mobile": "828-867-0067",
      "company": "Rooxo"
    }, {
      "id": 15,
      "name": "Enid Jouannisson",
      "email": "ejouannissone@gnu.org",
      "mobile": "645-407-2000",
      "company": "Skajo"
    }, {
      "id": 16,
      "name": "Ddene Tinsley",
      "email": "dtinsleyf@youku.com",
      "mobile": "562-877-3255",
      "company": "Topicstorm"
    }, {
      "id": 17,
      "name": "Raff Whiteman",
      "email": "rwhitemang@goo.ne.jp",
      "mobile": "579-414-1566",
      "company": "Twinte"
    }, {
      "id": 18,
      "name": "Kaleena Pettis",
      "email": "kpettish@hibu.com",
      "mobile": "828-669-6160",
      "company": "Teklist"
    }, {
      "id": 19,
      "name": "Lian Teek",
      "email": "lteeki@w3.org",
      "mobile": "392-449-5215",
      "company": "Eabox"
    }, {
      "id": 20,
      "name": "Cathrin Kibble",
      "email": "ckibblej@noaa.gov",
      "mobile": "720-749-5687",
      "company": "Jaxworks"
    }, {
      "id": 21,
      "name": "Karissa D'Ruel",
      "email": "kdruelk@imdb.com",
      "mobile": "919-903-7186",
      "company": "Devify"
    }, {
      "id": 22,
      "name": "Jasper Canwell",
      "email": "jcanwelll@ebay.com",
      "mobile": "959-408-7831",
      "company": "Voomm"
    }, {
      "id": 23,
      "name": "Nate Arkley",
      "email": "narkleym@washingtonpost.com",
      "mobile": "726-921-7979",
      "company": "Layo"
    }, {
      "id": 24,
      "name": "Barbey Furmonger",
      "email": "bfurmongern@lycos.com",
      "mobile": "846-351-1817",
      "company": "Buzzster"
    }, {
      "id": 25,
      "name": "Ceciley Kaaskooper",
      "email": "ckaaskoopero@statcounter.com",
      "mobile": "384-326-7202",
      "company": "Jaxspan"
    }, {
      "id": 26,
      "name": "Edmund Porch",
      "email": "eporchp@admin.ch",
      "mobile": "636-425-0255",
      "company": "Kwimbee"
    }, {
      "id": 27,
      "name": "Rodolfo Farren",
      "email": "rfarrenq@rambler.ru",
      "mobile": "573-316-8301",
      "company": "Skyndu"
    }, {
      "id": 28,
      "name": "Glennie Alpin",
      "email": "galpinr@nytimes.com",
      "mobile": "376-399-2222",
      "company": "Oyondu"
    }, {
      "id": 29,
      "name": "Egon Bellison",
      "email": "ebellisons@bbc.co.uk",
      "mobile": "286-490-8067",
      "company": "Quamba"
    }, {
      "id": 30,
      "name": "Bertie Seabon",
      "email": "bseabont@booking.com",
      "mobile": "406-924-3816",
      "company": "Teklist"
    }, {
      "id": 31,
      "name": "Annabal Borchardt",
      "email": "aborchardtu@weather.com",
      "mobile": "444-903-9811",
      "company": "Realbridge"
    }, {
      "id": 32,
      "name": "Salvidor Gildersleaves",
      "email": "sgildersleavesv@narod.ru",
      "mobile": "461-512-8187",
      "company": "Centimia"
    }, {
      "id": 33,
      "name": "Boigie Slott",
      "email": "bslottw@google.pl",
      "mobile": "412-445-4861",
      "company": "Gigaclub"
    }, {
      "id": 34,
      "name": "Christalle MacMoyer",
      "email": "cmacmoyerx@wordpress.org",
      "mobile": "601-111-5875",
      "company": "Agivu"
    }, {
      "id": 35,
      "name": "Junie Soper",
      "email": "jsopery@youtube.com",
      "mobile": "407-223-8762",
      "company": "Avamm"
    }, {
      "id": 36,
      "name": "Abran Skalls",
      "email": "askallsz@netlog.com",
      "mobile": "387-631-0002",
      "company": "Shufflester"
    }, {
      "id": 37,
      "name": "Ilse Dashkovich",
      "email": "idashkovich10@cloudflare.com",
      "mobile": "568-627-4398",
      "company": "Mita"
    }, {
      "id": 38,
      "name": "Livvyy Dorant",
      "email": "ldorant11@washington.edu",
      "mobile": "517-885-6654",
      "company": "Brainbox"
    }, {
      "id": 39,
      "name": "Ximenes Pach",
      "email": "xpach12@flavors.me",
      "mobile": "625-490-2084",
      "company": "Skibox"
    }, {
      "id": 40,
      "name": "Tabbatha Rupert",
      "email": "trupert13@economist.com",
      "mobile": "627-172-0861",
      "company": "Feedspan"
    }, {
      "id": 41,
      "name": "Willi Ney",
      "email": "wney14@ucoz.com",
      "mobile": "771-196-9357",
      "company": "Shuffletag"
    }, {
      "id": 42,
      "name": "Norene Egleton",
      "email": "negleton15@fema.gov",
      "mobile": "302-920-0397",
      "company": "Eazzy"
    }, {
      "id": 43,
      "name": "Ninnette De Beneditti",
      "email": "nde16@geocities.com",
      "mobile": "165-439-5938",
      "company": "Youfeed"
    }, {
      "id": 44,
      "name": "Allegra Duchant",
      "email": "aduchant17@parallels.com",
      "mobile": "168-831-8199",
      "company": "Meemm"
    }, {
      "id": 45,
      "name": "Virginie Redsull",
      "email": "vredsull18@slate.com",
      "mobile": "259-350-8703",
      "company": "Devpoint"
    }, {
      "id": 46,
      "name": "Nannie Radish",
      "email": "nradish19@wp.com",
      "mobile": "407-162-3920",
      "company": "Leenti"
    }, {
      "id": 47,
      "name": "Kylen Sandy",
      "email": "ksandy1a@bandcamp.com",
      "mobile": "691-872-8659",
      "company": "Kare"
    }, {
      "id": 48,
      "name": "Conway Corbert",
      "email": "ccorbert1b@i2i.jp",
      "mobile": "752-201-7517",
      "company": "Realpoint"
    }, {
      "id": 49,
      "name": "Lian Dorgan",
      "email": "ldorgan1c@twitpic.com",
      "mobile": "233-470-7142",
      "company": "Quatz"
    }, {
      "id": 50,
      "name": "Julie Kilvington",
      "email": "jkilvington1d@npr.org",
      "mobile": "357-586-4489",
      "company": "Gigashots"
    }]

    this.cd.detectChanges();
    
  }

  ngOnInit() {
    console.log("ngOnInit")
   
  }
  dele(item){
    console.log(item)

  }


}

