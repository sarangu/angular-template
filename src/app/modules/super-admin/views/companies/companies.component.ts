import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CommonService } from 'src/app/utils/services/common.service';
import { SuperAdminService } from 'src/app/utils/services/super-admin.service';
@Component({
  selector: 'companies-dashboard',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit, AfterViewInit {
  @ViewChild('actions', { static: false }) actions: TemplateRef<any>;
  modalRef: BsModalRef;
  rows: any;
  columns = []
  submitted = false;
  submitted1 = false;
  cid;
  form = this.fb.group({
  	companyName: ['', Validators.required],
	current409aValuation: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
	lastRoundValuation: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
	fullyDilutedShareCount: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
  })
  editform = this.fb.group({
  	companyName: ['', Validators.required],
	current409AValuation: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
	lastRoundValuation: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
	fullyDilutedShareCount: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
  })
  constructor(private cd: ChangeDetectorRef, private common: CommonService,private superadmin:SuperAdminService, private modalService: BsModalService, private fb: FormBuilder) {
  }
  getCompanies() {
    this.superadmin.companiesAll().subscribe(res => {
      this.rows = res;
      this.rows = this.rows.reverse()
    })
  }
  ngAfterViewInit() {
    this.columns = [
      { prop: 'companyName', name: "Company Name" },
      { prop: 'lastRoundValuation', name: "Last Round Valuation" },
      { prop: 'current409aValuation', name: "Current 409A Valuation" },
      { prop: 'fullyDilutedShareCount', name: "Fully Diluted ShareCount" },
      { prop: 'actions', name: "Actions", cellTemplate: this.actions, headerClass: "text-right", cellClass: "text-right", sortable: false }
    ];
    this.cd.detectChanges();
  }
  ngOnInit() {
    this.getCompanies();
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  editModal(template: TemplateRef<any>,row) {
    this.modalRef = this.modalService.show(template);
    this.editform.patchValue(row)
    this.cid = row._id;
  }
  add(){
    console.log(this.form.status)
    this.submitted = true;
    if (this.form.status == 'VALID') {
      console.log('add')
      this.superadmin.companiesAdd(this.form.value).subscribe(res=>{
        this.modalRef.hide()
        this.getCompanies();
        this.common.toast.fire({
          icon: 'success',
          title: `${this.form.value.companyName} Added successfully`
        })
      })
    }
  }
  edit(){
    this.submitted1 = true;
    if (this.editform.status == 'VALID') {
      console.log('add')
      this.editform.get("current409AValuation").setValue(this.editform.get("current409AValuation").value.toString())
      this.editform.get("lastRoundValuation").setValue(this.editform.get("lastRoundValuation").value.toString())
      this.editform.get("fullyDilutedShareCount").setValue(this.editform.get("fullyDilutedShareCount").value.toString())
      console.log(this.editform.value)
      this.superadmin.companiesEdit(this.cid,this.editform.value).subscribe(res=>{
        this.modalRef.hide()
        this.getCompanies();
        this.common.toast.fire({
          icon: 'success',
          title: `${this.editform.value.companyName} Updated successfully`
        })
      })
    }
  }
  dele(id) {
    this.common.alert.fire({
      title: 'Are you sure?',
      text: "You won't to delete ",
      icon: 'warning',
      showCancelButton: true
    }).then((result) => {
      if (result.value) {
        this.superadmin.companiesDel(id).subscribe(res=>{
          this.getCompanies();
        })
      }
    })
  }
}
