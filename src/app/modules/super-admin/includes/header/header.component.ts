import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/utils/services/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
menu = false;
  constructor(private router:Router,private api:CommonService) { }

  ngOnInit() {
  }
  menuOpen() {
    this.menu = true;
  }
  menuClose(e) {
    this.menu = e;
  }
  logout(e){
    e.preventDefault();
    localStorage.removeItem('auth');
    this.router.navigate(['/sign-in']);
    this.api.toast.fire({
      icon: 'success',
      title: 'Signed out successfully' 
    })

  }

}
