import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment"
@Injectable({
  providedIn: "root"
})
export class AdminService {
  constructor(private http: HttpClient) { }

  values = [5.00, 7.50, 10.00, 12.50, 15.00, 17.50, 20.00, 22.50, 25.00, 27.50, 30.00];

  
  companiesAll() {
    return this.http.get(environment.apiUrl + "companies/all")
  }
  user() {
    return this.http.get(environment.apiUrl + "users/current")
  }
  // liquidSolution() {
  //   return this.http.get(environment.apiUrl + "liquidsolutions/all")
  // }
  liquidSolutionsAll() {
    return this.http.get(environment.apiUrl + "liquidsolutions/all")
  }
  liquidSolutionsAdd(data) {
    return this.http.post(environment.apiUrl + "liquidsolutions", data)
  }
  liquidSolutionsDelete(id) {
    return this.http.delete(environment.apiUrl + "liquidsolutions/"+id)
  }
  liquidSolutionsSingle(id) {
    return this.http.get(environment.apiUrl + "liquidsolutions/"+id)
  }
  liquidSolutionsUpdate(id,data) {
    return this.http.put(environment.apiUrl + "liquidsolutions/"+id,data)
  }
  liquidSolutionsChart(id,obj) {
    return this.http.post(environment.apiUrl + "liquidsolutions/funding/analysis/"+id,obj)
  }
}

