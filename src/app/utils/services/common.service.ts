import { Injectable, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: "root"
})
export class CommonService {
  patterns = {
    numberDecimal: "^[0-9]+([.][0-9]+)?$",
    number: "^[0-9]*$",
    alphabets: "^[a-zA-Z][a-zA-Z ]*$",
  }
  loaderr = new EventEmitter;
  sidebarEmmiter = new EventEmitter;
  currentYear = new Date().getFullYear();
  datePicker = {
    config: {
      dateInputFormat: 'DD/MM/YYYY',
      adaptivePosition: true,
      isAnimated: true
    }
  }
  alert = Swal.mixin({
    confirmButtonColor: '#00B7F4',
    cancelButtonColor: '#3c3c3c',
    cancelButtonText: 'No',
    confirmButtonText: 'Yes',
    width: 300
  })
  toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  paginate = {
    firstText: "<i class='la la-angle-double-left'></i>",
    lastText: "<i class='la la-angle-double-right'></i>",
    nextText: "<i class='la la-arrow-right'></i>",
    previousText: "<i class='la la-arrow-left'></i>"
  }
  states = [{"code": "AN","name": "Andaman and Nicobar Islands"},
  {"code": "AP","name": "Andhra Pradesh"},
  {"code": "AR","name": "Arunachal Pradesh"},
  {"code": "AS","name": "Assam"},
  {"code": "BR","name": "Bihar"},
  {"code": "CG","name": "Chandigarh"},
  {"code": "CH","name": "Chhattisgarh"},
  {"code": "DH","name": "Dadra and Nagar Haveli"},
  {"code": "DD","name": "Daman and Diu"},
  {"code": "DL","name": "Delhi"},
  {"code": "GA","name": "Goa"},
  {"code": "GJ","name": "Gujarat"},
  {"code": "HR","name": "Haryana"},
  {"code": "HP","name": "Himachal Pradesh"},
  {"code": "JK","name": "Jammu and Kashmir"},
  {"code": "JH","name": "Jharkhand"},
  {"code": "KA","name": "Karnataka"},
  {"code": "KL","name": "Kerala"},
  {"code": "LD","name": "Lakshadweep"},
  {"code": "MP","name": "Madhya Pradesh"},
  {"code": "MH","name": "Maharashtra"},
  {"code": "MN","name": "Manipur"},
  {"code": "ML","name": "Meghalaya"},
  {"code": "MZ","name": "Mizoram"},
  {"code": "NL","name": "Nagaland"},
  {"code": "OR","name": "Odisha"},
  {"code": "PY","name": "Puducherry"},
  {"code": "PB","name": "Punjab"},
  {"code": "RJ","name": "Rajasthan"},
  {"code": "SK","name": "Sikkim"},
  {"code": "TN","name": "Tamil Nadu"},
  {"code": "TS","name": "Telangana"},
  {"code": "TR","name": "Tripura"},
  {"code": "UK","name": "Uttarakhand"},
  {"code": "UP","name": "Uttar Pradesh"},
  {"code": "WB","name": "West Bengal"}]
  constructor(private http: HttpClient, private location: Location, private toastr: ToastrService) {
  }
  short(item) {
    var html = '';
    item.split(' ').map(x => {
      html += x.charAt(0).toUpperCase();
    })
    return html;
  }
  goBack(e) {
    e.preventDefault();
    this.location.back()
  }
  dateformat(val) {
    let input = val;
    let format = new Date(input);
    let date = `${format.getFullYear()}-${format.getMonth() + 1}-${format.getDate()}`;
    return date;
  }
  loader(item) {
    this.loaderr.emit(item);
  }
  loaderAction() {
    return this.loaderr;
  }
  sidebarEmmiterEmit() {
    this.sidebarEmmiter.emit('');
  }
  sidebarEmmiterSubscribe() {
    return this.sidebarEmmiter;
  }
  toster(message, title, type) {
    this.toastr[type](message, title);
  }
  setStorage(name, value) {
    document.cookie = `${name}=${value}; path=/;`;
  }
  getStorage(name) {
    var temp = document.cookie.split(";");
    var res = "";
    for (let i = 0; i < temp.length; i++) {
      if (temp[i].split("=")[0].trim() == name) {
        res = temp[i].split("=")[1];
      }
    }
    return res;
  }
  deleteStorage(name) {
    document.cookie = `${name}=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
  }
  auth(name) {
    return (this.getStorage(name) ? true : false)
  }
}
