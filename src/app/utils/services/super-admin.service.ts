import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment"
@Injectable({
  providedIn: "root"
})
export class SuperAdminService {
  constructor(private http: HttpClient) { }
  companiesAll(){
    return this.http.get(environment.apiUrl+"companies/all")
  }
  companiesAdd(data){
    return this.http.post(environment.apiUrl+"companies",data)
  }
  companiesEdit(id,data){
    return this.http.put(environment.apiUrl+"companies/"+id,data)
  }
  companiesDel(id){
    return this.http.delete(environment.apiUrl+"companies/"+id)
  }
  usersAll(){
    return this.http.get(environment.apiUrl+"users/all")
  }
}
