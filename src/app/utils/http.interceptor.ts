import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { CommonService } from './services/common.service';
@Injectable()
export class HI implements HttpInterceptor {
    constructor(private router: Router, private common: CommonService) {
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = this.common.getStorage('token') ? this.common.getStorage('token') : '';
        const authreq = req.clone({ setHeaders: { Authorization: token ,Accept:'application/json'} })
        return next.handle(authreq).pipe(
            catchError((error: HttpErrorResponse) => {
                let html = '';
                if (typeof error.error == 'object') {
                    Object.values(error.error).forEach((value, index) => {
                        html += "<div>";
                        html += "<i class='fabric'>&#62257;</i> ";
                        html += value;
                        html += "</div>";
                    })
                } else {
                    html += error.error;
                }
                if (error.status == 401) {
                    localStorage.removeItem('token');
                    this.router.navigate(['/sign-in']);
                    this.common.toster(html, 'Unauthorized', 'error')
                }
                if (error.status == 400) {
                    this.common.toster(html, 'Bad Request', 'error')
                }
                return throwError(error);
            })
        );
    }
}
