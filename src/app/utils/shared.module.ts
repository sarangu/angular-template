import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullscreenComponent } from '../widgets/fullscreen/fullscreen.component';
import { LayoutComponent } from '../widgets/layout/layout.component';
import { DrawerComponent } from '../widgets/drawer/drawer.component';
import { SearchComponent } from '../widgets/search/search.component';
import { DatepickerComponent } from '../widgets/datepicker/datepicker.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [
    FullscreenComponent,
    DrawerComponent,
    LayoutComponent,
    SearchComponent,
    DatepickerComponent
  ],
    
  imports: [
    CommonModule,
    BsDatepickerModule.forRoot()
  ],
  exports:[
    FullscreenComponent,
    DrawerComponent,
    LayoutComponent,
    SearchComponent,
    DatepickerComponent
  ]
})
export class SharedModule { }
